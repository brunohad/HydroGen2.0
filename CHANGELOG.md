# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).



## Unreleased
---

### New

### Changes

### Fixes

### Breaks


## 2.2.2 - (2022-02-16)
---

### Fixes
* fixed bug with operational energy during multiple days simulations


## 2.2.1 - (2021-03-29)
---

### Changes
* Documentation update

### Fixes
* Easier argument handling in hydrograph method


## 2.2.0 - (2021-02-23)
---

### New
* API function to generate hydrograph


### Fixes
* improved worflow for testing purposes
* bug fix in setup.py


## 2.1.1 (2021-02-23)
---

### Breaks
* First public release
