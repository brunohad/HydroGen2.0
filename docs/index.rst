.. HydroGen documentation master file, created by
   sphinx-quickstart on Tue Jul 17 14:02:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====================================
Welcome to the HydroGen documentation
=====================================

.. toctree::
   :maxdepth: 3

   IncludeFile
   code
