The full documentation (including autogenerated description of all methods) is available on ReadTheDocs_.

.. _ReadTheDocs: https://hydrogen20.readthedocs.io/en/latest/index.html


General
========
`HydroGen` This is a tool for the stochastic simulation of domestic hot water consumption. You can use it as an API, from a graphical user interface or from the command-line. is a hydrograph generator, i.e. a tool for the stochastic generation of domestic water consumption events. Domestic water events are generated as rectangular pulses according to user-provided probability distributions and consumption data. This package was developed in parallel with the WaterHub Modelica package (see project WaterHub_ for more information).

.. _WaterHub: https://gitlab.com/brunohad/WaterHub

`HydroGen` is still under development, you may expect some small bugs.

Installation
------------

`HydroGen` can be installed, together with its dependencies, on your python distribution using pip:

.. code-block:: sh

  pip install hydrogen


Citation
--------

We hope this package will be useful to your research! If you use it, please cite it as follows:

`B. Hadengue, A. Scheidegger, E. Morgenroth, T.A. Larsen, Modeling the Water-Energy Nexus in Households, Energy & Buildings (2020), doi: https://doi.org/10.1016/j.enbuild.2020.110262`


Usage
-----
Command-line
~~~~~~~~~~~~

A script is included in the hydrogen package. The script is added to the path during installation and can be run through the following command

.. code-block:: sh

  hydrogen initFile.in

To run an example, try for instance the command:

.. code-block:: sh

  hydrogen -p

This will run an example `initFile.in` and plot the generated flow curve for a shower during three days.

The following flags and options are available:

.. tabularcolumns:: |p{3cm}|p{7cm}|

.. csv-table::
   :file: docs/_files/flagtable.csv
   :header-rows: 1
   :class: longtable
   :widths: 1 3

Note that to create an output file `OutputFile.csv`, the command reads for instance

.. code-block:: sh

    hydrogen initFile.in -o OutputFile.csv

A summary of all options is displayed using the `-h` option:

.. code-block:: sh

    hydrogen -h

GUI (beta)
~~~~~~~~~~

A graphical user interface is available as a Beta version. The tool allows to create or modify input files, simulate stochastic water consumption and plot the resulting curves.

Visit the `GUI project page`_ to download the stand-alone distribution. You have to extract the distribution, locate `HydroGen-GUI.exe` and run it. The GUI contains embedded help if required.

.. _`GUI project page`: https://gitlab.com/brunohad/hydrogen-gui

API
~~~

`HydroGen` methods can be used as a Python module from Python scripts, for instance in the context of a Monte-Carlo simulation where the WaterHub Modelica Library and the HydroGen Python Modules are used synchronously to generate stochastic simulations of domestic hot water systems. You could for instance generate hydrographs using the following code:

.. code-block:: python

  # Import the main method for the generation of hydrographs
  from hydrogen.HydroGen import hydrograph  ## Imports HydroGen main methods

  # Import some useful tools for the handling of input and output files.
  import hydrogen.tools.tools as tt

  # In its simplest form
  hydrograph("path/to/initFile.in")

  # Or with additional options
  hydrograph("path/to/initFile.in", outputFile="path/to/outputFile.csv", verbose=True, plot=True, conv_modelica=False)

An automatically generated description of all available methods is available on ReadTheDocs_.


Initialisation File
-------------------

The input file, or initialisation file (`initFile.in` in the above command), includes all necessary information for `HydroGen` to generate domestic water consumption curves. It is essentially a JSON file containing all useful values, which `HydroGen` reads and assigns to variables. Additional information about the JSON format on Wikipedia_ and how it is used in a pythonic context here_.

.. _Wikipedia: https://en.wikipedia.org/wiki/JSON
.. _here: https://docs.python.org/2/library/json.html

Note: the stand-alone GUI is a good tool to create or modify initialisation files. Have a look on the `project's page`_

.. _`project's page`: https://gitlab.com/brunohad/hydrogen-gui

The following example will serve as reference. In the following sections, we describe each key and field one after another.

.. code-block:: json

  {
  "totSimTime": 86400,
  "simDays": 3,
  "nbInhabitants": 2,
  "operationEnergy": "True",
  },
  "distroFile":{
    "fileName": "Path/to/Frequency/Curves/AKA/distroFile.xlsx",
    "skip_rows": [0,1],
    "use_cols": "A:B, C"
  },
  "eventList": [
    {"type":"Shower",
    "usage":"AdultUsage",
    "nbEvents": {
      "method": "truncNormPerCap",
      "loc": 2,
      "scale":1,
      "lower":0,
      "upper":3
    },
    "flowDist":{
      "dist":"truncNorm",
      "loc":0.127,
      "scale":4.158,
      "upper":1,
      "lower":0.04
    },
    "volumeDist":{
      "dist":"loglogistic",
      "loc":55.97,
      "scale":2.828
    },
    "tempDist":{
      "dist":"normal",
      "loc":39,
      "scale":2
    },
    "opEnDist":{
      "dist":"truncNorm",
      "loc":0.35,
      "scale":0.0525,
      "upper":0.6,
      "lower":0.1
      }
    }
    ]
  }

Simulation variables
~~~~~~~~~~~~~~~~~~~~
``totSimTime``, ``simDays`` and ``nbInhabitants`` are general simulation variables. They are described as follows:

+-----------------+--------------+----------------------------------------------------------------------------+
|  **Variable**   |  **Type**    |   **Description**                                                          |
+-----------------+--------------+----------------------------------------------------------------------------+
| `totSimTime`    |  integer     | | *totSimTime designates the simulation time of one period*                |
|                 |              | | *i.e. the number of seconds over which the events*                       |
|                 |              | | *shall be generated on. Default is 86400 seconds, or 1 day.*             |
+-----------------+--------------+----------------------------------------------------------------------------+
| `simDays`       |  integer     | | *simDays is the number of consecutive days (or periods) the simulation*  |
|                 |              | | *should be run.*                                                         |
+-----------------+--------------+----------------------------------------------------------------------------+
| `nbInhabitants` |  integer     | | *nbInhabitants is the number of people living in the household.*         |
|                 |  or list     | | *The number of events of each type is based on nbInhabitants.*           |
|                 |  of floats   | | *If nbInhabitants is a list of floats, the final integer will be sampled*|
|                 |              | | *from a truncated normal distribution with [loc, scale, lower, upper]*   |
+-----------------+--------------+----------------------------------------------------------------------------+

distroFile
~~~~~~~~~~

In HydroGen, the number of events is sampled - or generated through a Poisson process - from frequency curves. In the case of sampling, we use an `inverse transform sampling`_ process based on the normalization of the frequency curves (= flow curves) provided by the user. In the case of Poisson processes, the frequency curve is used as is.

.. _`inverse transform sampling`: https://en.wikipedia.org/wiki/Inverse_transform_sampling

The ``distroFile`` key lets `HydroGen` compute the frequency curves from an excel file. ``distroFile`` contains the average time-resolved flows (liters per time-step). The format of the file should be similar to the following:

+-------+--------+----+----+-------------------------------------------------------------------------+
| time  | Shower | WC | ...| *description, not included in file*                                     |
+-------+--------+----+----+-------------------------------------------------------------------------+
|0      |   0.2  |0.26| ...| | *between t = 0s and t = 300s, average draw for shower = 0.2 liters*   |
|       |        |    |    | | *average draw for WC = 0.26 liters*                                   |
+-------+--------+----+----+-------------------------------------------------------------------------+
|300    |  0.1   |0.06| ...|                                                                         |
+-------+--------+----+----+-------------------------------------------------------------------------+
|600    |  0.1   |0.15| ...|                                                                         |
+-------+--------+----+----+-------------------------------------------------------------------------+
|...    | ...    |... | ...| *and so on until ``totSimTime``*                                        |
+-------+--------+----+----+-------------------------------------------------------------------------+

``distroFile`` contains three fields: ``fileName`` is a string containing the path to the excel file containing the flows, ``skip_rows`` is a list containing the row numbers that should **not** be read by the software (for instance to avoid busy headers), and ``use_cols`` is a string expressing the columns that should be taken into account ("B:G, J": use columns B to G, and column J). The first column should always be the time index (in seconds).

If you wish to control the timing of your events, you can set ``fileName`` to "None" (with "" marks), remove the keys ``skip_rows`` and ``use_cols`` and replace them by ``startTime`` (the time at which the first event should start, in seconds) and ``timeDiff`` (time difference between each event, in seconds).

operationEnergy
~~~~~~~~~~~~~~~

The optional ``operationEnergy`` key takes value "True" or "False" (including quotation marks). This key tells the software to sample, in addition to water events, a necessary operational energy for the device under consideration. Such devices may be dishwashers or washing machines, for instance. If ``operationEnergy`` is "True", the software samples a value for the required energy of each event, and distributes this absolute energy to a required power over the duration of the event. The sampling is performed based on the data given in the ``opEnDist`` key present in ``eventList`` (see below). The resulting output file ``OutputFile.csv`` will contain an - additional - fourth column containing the energy per second required by the appliance when an event is triggered. Please note that only specific appliances in the WaterHub Modelica Library are compatible with such files.

If ``operationEnergy`` is not present in the initialization file, the software default is "False".


eventList
~~~~~~~~~

``eventList`` describes the type of events that shall be generated. In the case of the above example: the software will generate `Shower` events. The events types are organized in a list, each component of the list containing six fields: ``type``, ``usage``, ``nbEvents``, ``flowDist``, ``volumeDist``, ``tempDist`` and ``opEnDist``.

* ``type`` contains a string and describe the event type. It should be the same as one column from ``distroFile``. Types can for instance be `Shower`, `WC`, `kitchen`, `Wash Basin`, etc.
* ``usage`` contains a string describing the event itself. No constraints are set, the ``usage`` can be `TeethBrush`, `Person1`, `shortSummerShower`, etc.
* ``nbEvents`` contains a necessary information for the sampling of the number of events. See below for more information.
* ``flowDist``, ``volumeDist``, ``tempDist`` and ``opEnDist`` describe the necessary distributions to generate the targeted event type:

  + ``flowDist`` is the flow distribution, i.e. what water flow is triggered by the user.
  + ``volumeDist`` is the volume distribution, i.e. how much water is withdrawn in total. The event duration is computed using the event flow and volume.
  + ``tempDist`` is the event temperature distribution (often normal).
  + ``opEnDist`` is the distribution describing the required operational energy *per event*. When ``operationEnergy`` is set to "False" or simply left out of the file, no operation energy is simulated.

Each distribution contains a ``dist`` field describing which distribution is best to sample the value from, and the necessary distribution parameters. The distributions are constructed with their NumPy_ equivalent, and hence are described with the same parameters. The implemented distributions the user can choose from are:

  + `loglogistic`: built from np.random.logistic. Takes arguments ``loc`` and ``scale``.
  + `weibull`: based on np.random.weibull. Takes arguments ``shape`` and ``scale``.
  + `gamma`: based on np.random.gamma. Takes arguments ``loc`` and ``scale``.
  + `normal`: based on np.random.normal. Takes arguments ``loc`` and ``scale``.
  + `truncNorm`: based on scipy.stats.truncnorm. Takes arguments ``loc``, ``scale``, ``lower`` and ``upper``.
  + `lognormal`: based on np.random.lognormal. Takes arguments ``mean`` and ``sigma``.
  + `constant`: returns the value ``value``, without sampling from any distribution.
  + `uniform`: based on np.random.uniform. Takes arguments ``low`` and ``high`` as boundaries.
  + `rayleigh`: based on np.random.rayleigh. Takes arguments ``scale``.
  + `list`: based on np.random.choice. Takes arguments ``list``. Optionally, an argument ``p`` (a list with same dimensions as ``list`` summing up to 1) can be passed on, describing the sampling probabilities.

.. _NumPy: https://docs.scipy.org/doc/numpy/reference/routines.random.html

nbEvents
********
Each ``event`` in ``eventList`` contains a ``nbEvents`` dictionary with necessary information for the sampling of the number of events. Various methods are implemented:

* ``truncNorm``: the number of events will be sampled from a truncated discrete normal distribution. The user has to provide the keys ``loc`` and ``scale``, which describe the underlying normal distribution, and ``lower`` and ``upper``, defining the boundaries for truncation.
* ``truncNormPerCap``: same as ``truncNorm``, but sampled for each member of the household and summed.
* ``lognormal``: The user provides the keys ``mean`` and ``sigma``.
* ``truncLognormal``: truncated lognormal distribution. Additionally to ``mean`` and ``sigma``, the user provides the keys ``upper`` and ``lower`` to set the upper and lower bounds, respectively.
* ``truncLognormalPerCap``: same as ``truncLognormal``, but sampled for each member of the households and summed.
* ``poissonPerCap``: choosing this method will sample the number of events via a Poisson process, using the given distribution file (see ``distroFile``) as basis for an inhomogeneous process. The number of events is sampled from a Poisson distribution with rate equal to the sum of the distribution (i.e. liters/day) times ``nbInhabitants`` divided by``avgEventVol``, the average event volume provided by the user. If this key is not present, the average volume is set to the sum of the distribution (i.e. 1 event per day).
* ``poisson``: same as above, but the distribution is not considered as a "per capita" quantity anymore but as an absolute quantity: in the computation of the rate, the distribution is not multiplied by ``nbInhabitants``.
* ``constant``: the number of events is set to the number contained in the ``value`` key.
* ``list``: sampling is done from a list using `numpy.random.choice()`. The list ``p`` provides the probabilities to pick each list member and must sum to 1. If you leave ``p`` empty, the list will be sampled uniformly.


Output
~~~~~~
You can ask `HydroGen` to produce a `.csv` or a `.txt` file that is meant to be fed into the WaterHub Modelica Package as input for appliance models. Modelica is quite picky concerning its accepted input files, it thus has the following format (here with default ``totSimTime = 86400`` and ``simDays = 3``, hence the value `259200` in the header):

.. code-block:: sh

    #1
    float FlowTable(259200, 3)
    0, 0.0, 0.0
    1, 0.0, 0.0
    2, 0.0, 0.0
      .
      .
      .
    27832, 0.1, 311.0    <- Event! flow is 0.1 l/s at 38°C (311 K) at time t = 27832 s
    27833, 0.1, 311.0
      .
      .
      .
    259199, 0.0, 0.0
    259200, 0.0, 0.0      <- End of simulation


Known Issues
------------
Please keep in mind that this program is still under development. Some weird bugs may be expected and we do not take any responsibility for them.
