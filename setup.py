import setuptools
import sys

def readme():
    with open('README_PyPi.rst') as f:
        return f.read()

install_requires = ["matplotlib<=2.2.5;python_version<'3.7'",
                    "scipy<=1.2.3;python_version<'3.7'",
                    "pandas<=0.24.2;python_version<'3.7'",
                    "numpy<=1.16.6;python_version<'3.7'",
                    "pyparsing <= 2.4.7;python_version<'3.7'",
                    "xlrd < 2.0;python_version<'3.7'",
                    "argparse;python_version<'3.7'",
                    "matplotlib>=2.2.5;python_version>='3.7'",
                    "scipy>=1.6;python_version>='3.7'",
                    "pandas >= 1.2.2;python_version>='3.7'",
                    "numpy >= 1.20.1;python_version>='3.7'",
                    "pyparsing >= 3.0.0b2;python_version>='3.7'",
                    "openpyxl >= 3.0.6;python_version>='3.7'"
                    ]

setuptools.setup(
      name='hydrogen',
      version='2.2.2',
      description='Hydrograph generator: generate stochastic domestic water consumption events',
      long_description=readme(),
      url='https://gitlab.com/brunohad/HydroGen2.0',
      author='Bruno Hadengue',
      author_email='bruno.hadengue@eawag.ch',
      license='GNU AGPLv3',
      packages=setuptools.find_packages(),
      install_requires=install_requires,
      entry_points ={
            'console_scripts': [
                'hydrogen = hydrogen.HydroGen:main']
      },
      test_suite='nose.collector',
      tests_require=['nose'],
      zip_safe=False,
      include_package_data=True,
      options={"bdist_wheel": {"universal": "1"}})
